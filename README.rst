==========
Podomation
==========

:author: Curtis Sand
:author_email: curtissand@gmail.com
:repository: https://github.com/fretboardfreak/podomation.git
:mirror: https://bitbucket.org/fret/podomation.git

Podomation is a tool and a set of resources that helps me configure and use various useful
containers.

Design Notes
============

Since I am using Fedora 32 and later, my main target is the podman containerization service. Podman
is fairly good about maintaining compatibility with Docker so there may be some free compatibility
with Docker. However in the multi-container cases Podman uses Kubernetes Pods rather than
docker-compose files so it is possible not every feature will be available for Docker.

Use Cases
---------

#. build my customized docker containers using a standard set of options
#. configure and prepare pods
#. shortcuts for commands to inspect pods and other maintenance
